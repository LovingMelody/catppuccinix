inputs:
let inherit inputs;
in self: super: {
  catppuccin-hyprland = super.callPackage ../packages/catppuccin-hyprland { };
  catppuccin-spicetify = super.callPackage ../packages/catppuccin-spicetify { };
  catppuccin-cava = super.callPackage ../packages/catppuccin-cava { };
  catppuccin-discord = super.callPackage ../packages/catppuccin-discord { };
  catppuccin-bat = super.callPackage ../packages/catppuccin-bat { };
  catppuccin-starship = super.callPackage ../packages/catppuccin-starship { };
  catppuccin-base16 = super.callPackage ../packages/catppuccin-base16 { };
}
