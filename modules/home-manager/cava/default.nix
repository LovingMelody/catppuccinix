{ lib, config, pkgs, ... }:
with lib;
let cfg = config.catppuccinix.targets.cava;
in {
  options.catppuccinix.targets.cava.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Enable cava theme";
  };

  config = mkIf cfg.enable {
    home.file.".config/cava/config".source =
      "${pkgs.catppuccin-cava}/share/themes/${toLower config.catppuccinix.flavor}.cava";
  };
}
