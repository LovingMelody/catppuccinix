{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.catppuccinix.targets.starship;
in
{
  options.catppuccinix.targets.starship.enable = mkOption {
    type = types.bool;
    default = config.programs.starship.enable;
    description = "Enable starship prompt themes";
  };

  config = mkIf cfg.enable {
    programs.starship = {
      settings = {
        palette = "catppuccin_${toLower config.catppuccinix.flavor}";
      } // builtins.fromTOML (builtins.readFile
        "${pkgs.catppuccin-starship}/share/palettes/${toLower config.catppuccinix.flavor}.toml");
    };
  };
}
