{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.catppuccinix;
  lut = ./LUT;
in
{
  options.catppuccinix = {
    enable = mkEnableOption "catppuccinix theming";
    flavor = mkOption {
      type = types.enum [ "Latte" "Frappe" "Macchiato" "Mocha" ];
      default = "Macchiato";
    };
    # TODO: Support accent colors
    image = {
      # Don't know how to make the value nullable
      # So we can use a default value thats a string
      path = mkOption {
        type = types.coercedTo types.package toString types.path;
        default = "${pkgs.pantheon.elementary-wallpapers}/backgrounds/Julia Craice.jpg";
        description = "Path to the image to use as the background";
      };
      editImage = mkOption {
        type = types.bool;
        default = false;
        description = "Whether to edit the image to fit the theme";
      };
      out = mkOption {
        type = types.path;
        default = null;
        description =
          "Path to the output image of the edit, not meant to be set by the user";
      };
    };
  };

  config = mkIf cfg.enable {
    catppuccinix.image.out =
      if config.catppuccinix.image.editImage then
        (pkgs.runCommand "background.png" { }
          "${pkgs.imagemagick}/bin/magick '${config.catppuccinix.image.path}' '${lut}/${
            toLower config.catppuccinix.flavor}-hald-clut.png' -hald-clut $out")
      else
        config.catppuccinix.image.path;
  };
}
