{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-discord";
  version = "2022-10-02";

  # Use commit from last change to theme
  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "discord";
    rev = "b9f42df1b9b38957a4d8b7e74637a5480be9be57";
    sha256 = "ArhHQSuXc04Tti7pd5EfScm7szhBmS1hcEOE9q+wj/E=";
  };

  meta = {
    description = "Catppuccin theme for discord";
    homepage = "https://github.com/catppuccin/discord";
  };

  installPhase = ''
    mkdir -p $out/share/
    cp -r ${src}/themes $out/share/themes
  '';
}
