{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-cava";
  version = "2022-10-11";

  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "cava";
    rev = "ad3301b50786e22e31cbf4316985827d6f05845e";
    sha256 = "hYC6ExtroRy2UoxGNHAzKm9MlTdJSegUWToat4VoN20=";
  };

  meta = {
    description = "Catppuccin theme for spicetify-cli";
    homepage = "https://github.com/catppuccin/spicetify";
  };

  installPhase = ''
    mkdir -p $out/share/themes
    cp -r ${src}/* $out/share/themes/
  '';
}
