{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-starship";
  version = "2023-02-21";

  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "starship";
    rev = "3e3e54410c3189053f4da7a7043261361a1ed1bc";
    sha256 = "soEBVlq3ULeiZFAdQYMRFuswIIhI9bclIU8WXjxd7oY=";
  };

  meta = {
    description = "Catppuccin theme for starship";
    homepage = "https://github.com/catppuccin/starship";
  };

  installPhase = ''
    mkdir -p $out/share/
    cp -r ${src}/palettes $out/share/palettes
  '';
}
