{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-bat";
  version = "2022-11-10";

  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "bat";
    rev = "ba4d16880d63e656acced2b7d4e034e4a93f74b1";
    sha256 = "6WVKQErGdaqb++oaXnY3i6/GuH2FhTgK0v4TN4Y0Wbw=";
  };

  meta = {
    description = "Catppuccin theme for bat";
    homepage = "https://github.com/catppuccin/bat";
  };

  installPhase = ''
    mkdir -p $out/share/themes
    cp ${src}/*.tmTheme $out/share/themes/
  '';
}
