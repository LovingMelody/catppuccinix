{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-spicetify";
  version = "2023-05-10";

  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "spicetify";
    rev = "fb7ef5a8d7b66d40e916e86d3ffb3b00d404ac76";
    sha256 = "yitiDqXZgX5NQlX66FW4YgS5fO2eF1W0QX5rKLD9uRE=";
  };

  meta = {
    description = "Catppuccin theme for spicetify-cli";
    homepage = "https://github.com/catppuccin/spicetify";
  };

  installPhase = ''
    mkdir -p $out/share/themes
    cp -r ${src}/* $out/share/themes
  '';
}
