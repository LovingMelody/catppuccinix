{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "catppuccin-base16";
  version = "2022-10-12";

  src = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "base16";
    rev = "ca74b4070d6ead4213e92da1273fcc1853de7af8";
    sha256 = "fZDsmJ+xFjOJDoI+bPki9W7PEI5lT5aGoCYtkatcZ8A=";

  };

  meta = {
    description = "Base16 Catppuccin theme";
    homepage = "https://github.com/catppuccin/base16";
  };

  installPhase = ''
    mkdir -p $out/share/
    cp -r ${src}/base16 $out/share/themes
  '';
}
