{
  description = "Set Catppuccin themes for full system";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, ... }@inputs:
    with inputs;
    {
      overlays.default = final: prev: (import ./overlays inputs) final prev;
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;
      formatter.x86_64-darwin =
        nixpkgs.legacyPackages.x86_64-darwin.nixpkgs-fmt;
      formatter.aarch64-linux =
        nixpkgs.legacyPackages.aarch64-linux.nixpkgs-fmt;

      internals = {
        modules = {
          generic = ./modules/generic;
          homeManager = ./modules/home-manager;
        };
      };
      # NixOS Specific configuration
      # nixosModules.catppuccin = { pkgs, ... }@args: { };

      # Home Manager Specific configuration
      homeManagerModules.catppuccinix = with self.internals.modules;
        { pkgs, ... }@args: {
          imports = [
            self.internals.modules.generic
            self.internals.modules.homeManager
          ];
        };

      # Method to test a module
      testModule = { pkgs, ... }@args: {
        imports = [
          self.internals.modules.generic
          self.internals.modules.homeManager
          ./modules/test
        ];
      };
      # Usage: nix run .#testModule

      # Darwin Specific configuration
      # darwinModules.catppuccin = { pkgs, ... }@args: { };

    } // (flake-utils.lib.eachSystem [
      "x86_64-linux"
      "x86_64-darwin"
      "aarch64-linux"
    ]) (system:
      let pkgs = nixpkgs.legacyPackages.${system}.extend self.overlays.default;
      in {
        packages = flake-utils.lib.flattenTree {
          catppuccin-hyprland = pkgs.catppuccin-hyprland;
          catppuccin-spicetify = pkgs.catppuccin-spicetify;
          catppuccin-cava = pkgs.catppuccin-cava;
          catppuccin-discord = pkgs.catppuccin-discord;
          catppuccin-bat = pkgs.catppuccin-bat;
          catppuccin-starship = pkgs.catppuccin-starship;
          catppuccin-base16 = pkgs.catppuccin-base16;
        };
      });
}
